Categories:Security
License:GPLv3+
Web Site:https://github.com/squarrel/WrongPinShutdown/blob/HEAD/README.md
Source Code:https://github.com/squarrel/WrongPinShutdown
Issue Tracker:https://github.com/squarrel/WrongPinShutdown/issues
Donate:https://www.paypal.com/cgi-bin/webscr?cmd=_donations&business=admin%40snslocation%2ecom&lc=US&item_name=Wrong%20Pin%20Shutdown&currency_code=USD&bn=PP%2dDonationsBF%3abtn_donateCC_LG%2egif%3aNonHosted

Auto Name:Wrong Pin Shutdown
Summary:Shutdown after 10 unsuccessful logins
Description:
Shut down your device after 10 unsuccessful attempts to unlock it.

The app opens a paypal donation website at startup.

The method used for shutting down the phone may not work on all phones
and OS versions. Please make sure you follow the instructions before use.
.

Repo Type:git
Repo:https://github.com/squarrel/WrongPinShutdown

Requires Root:Yes

Build:1.0,1
    commit=33ee07dae4831c56987cd44b35a69ad8931ddcf0

Maintainer Notes:
Maybe we should remove the paypal intent from src/com/pikselbit/wrongpinshutdown/MainActivity.java ?
.

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:1.0
Current Version Code:1

